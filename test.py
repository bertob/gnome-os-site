import http.server
import pathlib

class MyHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        # Get the requested path
        requested_path = pathlib.Path(self.path.lstrip('/'))

        # If it's a directory, serve index.html
        if self.path.endswith('/'):
            self.path += 'index.html'

        # If the file doesn't exist and doesn't have an extension, try adding .html
        elif not requested_path.suffix and not requested_path.exists():
            self.path += '.html'

        return super().do_GET()

if __name__ == '__main__':
    http.server.test(HandlerClass=MyHandler)

